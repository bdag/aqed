open_project vitis -reset

add_files -cflags "-Wall -I../../../../include -I../../../../thirdparty/ac_types-4.0/include" -csimflags "-Wall -I../../../../include -I../../../../thirdparty/ac_types-4.0/include" openTitanHmacAqed.cpp
add_files -cflags "-Wall -I../../../../include -I../../../../thirdparty/ac_types-4.0/include" -csimflags "-Wall -I../../../../include -I../../../../thirdparty/ac_types-4.0/include" -tb tb.cpp
add_files -blackbox hmacHlsAdapter.json

open_solution default
set_part artix7
set_top OpenTitanHmacAqedTop
config_rtl -reset all -reset_async
config_interface -register_io scalar_all

csim_design
csynth_design

# The TileLink RTL wrappers include HLS co-simulation-only implementations as well as the real A-QED
# implementations within the same source file so that we don't have to bring in all the
# design-specific IP just for HLS / co-simulation. The implementation used at elaboration /
# simulation time is controlled by definition guards within the source files. It is expected that
# the Verilog elaborator / compiler passes the appropriate definitions to control which
# implementation is chosen.
#
# However, since we can't pass elaboration flags (to select the co-simulation-only implementation)
# directly using cosim_design, we have to first generate the simulation files and modify the
# elaboration flags before we run the co-simulation.
# TODO
#cosim_design
cosim_design -trace_level all -wave_debug

exit