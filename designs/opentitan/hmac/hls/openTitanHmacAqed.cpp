#include "aqed.h"

#include "openTitanHmacAqed.h"

namespace Aqed {
    namespace OpenTitan {
        namespace HMAC {

            void Top(size_t batch_size, ap_bool is_original, size_t original_index, ap_bool is_duplicate, size_t duplicate_index, Aqed::dut_input dut_in[Aqed::kMaxBatchSize], AqedOutput& top_output) {
                #pragma HLS array_partition variable=dut_in type=complete
                #pragma HLS interface mode=ap_none port=top_output
                #pragma HLS disaggregate variable=top_output

                static Aqed aqed;

                Aqed::AqedInput top_input{batch_size, is_original, original_index, is_duplicate, duplicate_index};
                for (size_t i = 0; i < Aqed::kMaxBatchSize; i++) {
                    #pragma HLS unroll
                    top_input.dut_in[i] = dut_in[i];
                }
                
                top_output = aqed(top_input);
            }
        }
    }   
}

// Because Vivado / Vitis HLS doesn't understand namespaces...
void OpenTitanHmacAqedTop(Aqed::size_t batch_size, Aqed::ap_bool is_original, Aqed::size_t original_index, Aqed::ap_bool is_duplicate, Aqed::size_t duplicate_index, Aqed::OpenTitan::HMAC::Aqed::dut_input dut_in[Aqed::OpenTitan::HMAC::Aqed::kMaxBatchSize], Aqed::AqedOutput& top_output) {
    Aqed::OpenTitan::HMAC::Top(batch_size, is_original, original_index, is_duplicate, duplicate_index, dut_in, top_output);
}

void HmacHlsAdapter(
    Aqed::OpenTitan::HMAC::TileLinkAdapterAddress& address_i, 
    Aqed::OpenTitan::HMAC::TileLinkAdapterWriteEnable& we_i,
    Aqed::OpenTitan::HMAC::TileLinkAdapterData& data_i,
    Aqed::OpenTitan::HMAC::TileLinkAdapterSize& size_i,
    Aqed::OpenTitan::HMAC::TileLinkAdapterData& data_o,
    Aqed::OpenTitan::HMAC::TileLinkAdapterDataIntegrity& data_integrity_o,
    Aqed::OpenTitan::HMAC::TileLinkAdapterError& error_o,
    Aqed::OpenTitan::HMAC::TileLinkAdapterError& integrity_error_o) {
    #pragma HLS inline off
    // Real implementation in RTL
    data_o = data_i;
    data_integrity_o = -1;
    error_o = 0;
    integrity_error_o = 0;
}