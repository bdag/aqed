#pragma once

#ifndef __SYNTHESIS__
#include <iostream>
#endif

#include <ap_int.h>

#include "aqed.h"

namespace Aqed {
    namespace OpenTitan {
        namespace HMAC {
            
            static constexpr int kInputWidth = 67;
            static constexpr int kOutputWidth = 41;

            struct OpenTitanHmacDut;
            using Aqed = Aqed<kInputWidth, kOutputWidth, 1, 16, OpenTitanHmacDut>;

            using TileLinkAdapterAddress       = ap_uint<32>;
            using TileLinkAdapterWriteEnable   = ap_uint<1>;
            using TileLinkAdapterData          = ap_uint<32>;
            using TileLinkAdapterSize          = ap_uint<2>;
            using TileLinkAdapterDataIntegrity = ap_uint<7>;
            using TileLinkAdapterError         = ap_uint<1>;
        }
    }
}

// TileLink RTL wrappers
void HmacHlsAdapter(
    Aqed::OpenTitan::HMAC::TileLinkAdapterAddress& address_i, 
    Aqed::OpenTitan::HMAC::TileLinkAdapterWriteEnable& we_i,
    Aqed::OpenTitan::HMAC::TileLinkAdapterData& data_i,
    Aqed::OpenTitan::HMAC::TileLinkAdapterSize& size_i,
    Aqed::OpenTitan::HMAC::TileLinkAdapterData& data_o,
    Aqed::OpenTitan::HMAC::TileLinkAdapterDataIntegrity& data_integrity_o,
    Aqed::OpenTitan::HMAC::TileLinkAdapterError& error_o,
    Aqed::OpenTitan::HMAC::TileLinkAdapterError& integrity_error_o);

namespace Aqed {
    namespace OpenTitan {
        namespace HMAC {

            struct OpenTitanHmacDut {
                static void Call(Aqed::Batch* batch) {
                    #ifndef __SYNTHESIS__
                    std::cout << "TileLink RTL wrapper call" << std::endl;
                    #endif

                    // Slice input
                    TileLinkAdapterAddress     address_i = batch->inputs[0](31, 0);
                    TileLinkAdapterWriteEnable we_i      = batch->inputs[0][32];
                    TileLinkAdapterData        data_i    = batch->inputs[0](64, 33);
                    TileLinkAdapterSize        size_i    = batch->inputs[0](66, 65);

                    // Make space for output
                    
                    TileLinkAdapterData          data_o            = 0;
                    TileLinkAdapterDataIntegrity data_integrity_o  = 0;
                    TileLinkAdapterError         error_o           = 0;
                    TileLinkAdapterError         integrity_error_o = 0;

                    // Call
                    HmacHlsAdapter(address_i, we_i, data_i, size_i,
                                    data_o, data_integrity_o, error_o, integrity_error_o);

                    // Concatenate output
                    batch->outputs[0] = (((integrity_error_o, error_o), data_integrity_o), data_o);
                }
            };

            void Top(size_t batch_size, ap_bool is_original, size_t original_index, ap_bool is_duplicate, size_t duplicate_index, Aqed::dut_input dut_in[Aqed::kMaxBatchSize], AqedOutput& top_output);
        }
    }
}

// Because Vivado / Vitis HLS doesn't understand namespaces...
void OpenTitanHmacAqedTop(Aqed::size_t batch_size, Aqed::ap_bool is_original, Aqed::size_t original_index, Aqed::ap_bool is_duplicate, Aqed::size_t duplicate_index, Aqed::OpenTitan::HMAC::Aqed::dut_input dut_in[Aqed::OpenTitan::HMAC::Aqed::kMaxBatchSize], Aqed::AqedOutput& top_output);