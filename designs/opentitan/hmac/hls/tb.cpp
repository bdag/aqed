#include <iostream>

#include "aqed.h"
#include "openTitanHmacAqed.h"

namespace Dut = Aqed::OpenTitan::HMAC;
using AqedDut = Dut::Aqed;

int main() {
	using std::cout;
	using std::endl;

    AqedDut::AqedInput input;
    Aqed::AqedOutput output;

	cout << "======================================" << endl;
	cout << "=== OpenTitan HMAC A-QED Testbench ===" << endl;
	cout << "======================================" << endl;
    cout << "# A-QED Parameters" << endl;
    AqedDut::PrintConstants();
    cout << endl;

	cout << "# INPUT 1" << endl;
    input.batch_size      = 1;
    input.is_original     = true;
    input.original_index  = 0;
    input.is_duplicate    = 0;
    input.duplicate_index = 0;

    input.dut_in[0]         = 0xBDA6BDA6BDA6BDA6;
    input.dut_in[0](66, 65) = 0xBDA6;
	input.Print();
    cout << endl;
	
    cout << "# A-QED CALL 1" << endl;
    OpenTitanHmacAqedTop(input.batch_size, input.is_original, input.original_index, input.is_duplicate, input.duplicate_index, input.dut_in, output);
    cout << endl;

    cout << "# OUTPUT 1" << endl;
	output.Print();
    cout << endl;

    cout << "# INPUT 2" << endl;
    input.is_original  = false;
    input.is_duplicate = false;

    input.dut_in[0] = 0x123456789ABCDEF;
	input.Print();
    cout << endl;

    cout << "# A-QED CALL 2" << endl;
	OpenTitanHmacAqedTop(input.batch_size, input.is_original, input.original_index, input.is_duplicate, input.duplicate_index, input.dut_in, output);
    cout << endl;

    cout << "# OUTPUT 2" << endl;
	output.Print();
    cout << endl;

    cout << "# INPUT 3" << endl;
    input.is_original  = false;
    input.is_duplicate = true;

    input.dut_in[0]         = 0xBDA6BDA6BDA6BDA6;
    input.dut_in[0](66, 65) = 0xBDA6;
	input.Print();
    cout << endl;

    cout << "# A-QED CALL 3" << endl;
	OpenTitanHmacAqedTop(input.batch_size, input.is_original, input.original_index, input.is_duplicate, input.duplicate_index, input.dut_in, output);
    cout << endl;

    cout << "# OUTPUT 3" << endl;
	output.Print();

	return (output.is_fc) ? 0 : 1;
}