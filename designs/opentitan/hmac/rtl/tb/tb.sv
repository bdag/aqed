module AqedTb
    ();

    logic clk = 0;
    logic rstn = 0;
    logic ap_start = 0;

    logic[63:0] batch_size = 1;
    logic is_original = 0;
    logic[63:0] original_index = 0;
    logic is_duplicate = 0;
    logic[63:0] duplicate_index = 0;

    logic[31:0] address = 0;
    logic we = 0;
    logic[31:0] data = 0;
    logic[1:0] size = 0;

    wire[66:0] dut_in;

    assign dut_in = {size, data, we, address};

    /************************
    * Module Instantiations *
    *************************/
    AqedTopWrapper aqed_top_wrapper(
        .clk_i(clk),
        .rstn_i(rstn),
        .ap_start_i(ap_start),
        .batch_size_i(batch_size),
        .is_original_i(is_original),
        .original_index_i(original_index),
        .is_duplicate_i(is_duplicate),
        .duplicate_index_i(duplicate_index),
        .dut_in_i(dut_in)
    );

    /***********
    * Clocking *
    ************/
    default clocking tb_clock @(posedge clk); endclocking

    /************
    * Processes *
    *************/
    /* Clocking */
    always #1 clk = ~clk;

    /* Test */
    initial begin
        /* Reset */
        rstn = 0;
        ##11;   // Hold reset for 100 cycles
        rstn = 1;
        ##1;

        /* Send one input */
        address = 'h 18;
        is_original = 1;
        ap_start = 1;

        ##1 ap_start = 0;

        wait (aqed_top_wrapper.ap_ready_o);

        is_original = 0;
        ap_start = 0;

        wait (!aqed_top_wrapper.ap_ready_o);

        ap_start = 1;
        is_duplicate = 1;

        ##2 ap_start = 0;
        
        /* Terminate */
        ##50 $finish;
    end
endmodule