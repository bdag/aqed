module HmacAdapter import tlul_pkg::*, prim_mubi_pkg::mubi4_t;
    (
        input wire clk_i,
        input wire rstn_i,

        input wire req_i,                        // Request valid
        input wire[top_pkg::TL_AW-1:0] addr_i,   // Request address
        input wire we_i,                         // Request type (0 = Get, 1 = Put*)
        input wire[top_pkg::TL_DW-1:0] wdata_i,  // Write data
        input wire[top_pkg::TL_DBW-1:0] be_i,    // Size (2^n)

        output wire gnt_o,                            // DUT ready
        output wire valid_o,                          // Response valid
        output wire[top_pkg::TL_DW-1:0] rdata_o,      // Response data
        output wire[DataIntgWidth-1:0] rdata_intg_o,  // Response data integrity
        output wire err_o,                            // Response error
        output wire intg_err_o,                       // Response integerity error

        output logic intr_hmac_done_o,
        output logic intr_fifo_empty_o,
        output logic intr_hmac_err_o,

        output prim_mubi_pkg::mubi4_t idle_o
    );

    mubi4_t instr_type_i = prim_mubi_pkg::MuBi4False;   // Request instruction type
    
    /* HMAC <-> TL Adapter */
    tlul_pkg::tl_h2d_t h2d;
    tlul_pkg::tl_d2h_t d2h;

    /************************
    * Module Instantiations *
    *************************/
    hmac hmac(
        .clk_i,
        .rst_ni(rstn_i),
        .tl_i(h2d),
        .tl_o(d2h),
        .intr_hmac_done_o,
        .intr_fifo_empty_o,
        .intr_hmac_err_o,
        .idle_o
    );

    tlul_adapter_host #(
        .MAX_REQS(1),
        .EnableDataIntgGen(1),
        .EnableRspDataIntgCheck(1)
    ) tlul_adapter (
        .clk_i,
        .rst_ni(rstn_i),
        .req_i,
        .gnt_o,
        .addr_i,
        .we_i,
        .wdata_i,
        .be_i,
        .instr_type_i,
        .valid_o,
        .rdata_o,
        .rdata_intg_o,
        .err_o,
        .intg_err_o,
        .tl_o(h2d),
        .tl_i(d2h)
    );
endmodule