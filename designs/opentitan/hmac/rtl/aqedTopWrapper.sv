module AqedTopWrapper(
    input wire clk_i,
    input wire rstn_i,
    input wire ap_start_i,

    input wire[63:0] batch_size_i,
    input wire is_original_i,
    input wire[63:0] original_index_i,
    input wire is_duplicate_i,
    input wire[63:0] duplicate_index_i,
    input wire[66:0] dut_in_i,

    output wire ap_done_o,
    output wire ap_idle_o,
    output wire ap_ready_o,

    output wire is_original_labelled_o,
    output wire is_original_labelled_valid_o,
    output wire is_original_done_o,
    output wire is_original_done_valid_o,
    output wire is_duplicate_done_o,
    output wire is_duplicate_done_valid_o,
    output wire is_fc_o,
    output wire is_fc_valid_o
);

    /************************
    * Module Instantiations *
    *************************/
    /* HMAC adapter (DUT) */
    HmacAdapter dut(
        .clk_i(clk_i),
        .rstn_i(rstn_i)
    );

    /* A-QED */
    OpenTitanHmacAqedTop aqed(
        .ap_clk(clk_i),
        .ap_rst(~rstn_i),
        .ap_start(ap_start_i),
        
        .batch_size(batch_size_i),
        .is_original(is_original_i),
        .original_index(original_index_i),
        .is_duplicate(is_duplicate_i),
        .duplicate_index(duplicate_index_i),
        .dut_in(dut_in_i),

        .ap_done(ap_done_o),
        .ap_idle(ap_idle_o),
        .ap_ready(ap_ready_o),

        .top_output_is_original_labelled(is_original_labelled_o),
        .top_output_is_original_labelled_ap_vld(is_original_labelled_valid_o),
        .top_output_is_original_done(is_original_done_o),
        .top_output_is_original_done_ap_vld(is_original_done_valid_o),
        .top_output_is_duplicate_done(is_duplicate_done_o),
        .top_output_is_duplicate_done_ap_vld(is_duplicate_done_valid_o),
        .top_output_is_fc(is_fc_o),
        .top_output_is_fc_ap_vld(is_fc_valid_o)
    );

    // Signals between DUT and A-QED are "magically" connected in TileLink adapter wrappers.

    /*************
    * Properties *
    **************/
    // Main FC property
    assert_fc_property : assert property (@(posedge clk_i)
        (is_original_done_o && is_duplicate_done_o) |-> is_fc_o);

    //Batch size is always 1
    assume_batch_size : assume property (@(posedge clk_i)
        batch_size_i == 1);

    // Original and duplicate indices are always 0 (single-mode accelerator)
    assume_single_mode : assume property (@(posedge clk_i)
        (original_index_i == 0) && (duplicate_index_i == 0));
endmodule