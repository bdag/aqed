`timescale 1 ns / 1 ps

module HmacHlsAdapter(
    input clk_i,
    input rst_i,
    input ap_ce,
    output wire ap_idle,
    input ap_start,
    input ap_continue,
    output wire ap_done,
    output wire ap_ready,

    input wire[31:0] address_i,
    input wire we_i,
    input wire[31:0] data_i,
    input wire[1:0] size_i,

    output wire[31:0] data_o,
    output wire data_valid_o,
    output wire[6:0] data_integrity_o,
    output wire data_integrity_valid_o,
    output wire error_o,
    output wire error_valid_o,
    output wire integrity_error_o,
    output wire integrity_error_valid_o
);

`ifdef XILINX_SIMULATOR
    /*******************************
    * Co-simulation Implementation *
    ********************************/
    assign ap_idle = 1;
    assign ap_done = 1;
    assign ap_ready = 1;

    assign data_o = data_i;
    assign data_valid_o = 1;
    assign data_integrity_o = -1;
    assign data_integrity_valid_o = 1;
    assign error_o = 0;
    assign error_valid_o = 1;
    assign integrity_error_o = 0;
    assign integrity_error_valid_o = 1;

    initial begin
        $display("TileLinkAdapterWrapperCall: Using HLS co-simulation implementation");
    end
`else
    /**********************
    * Real Implementation *
    ***********************/
    // Handshake signals
    assign ap_idle = (dut.idle_o == 'h 6);
    assign ap_done = 1;
    assign ap_ready = (dut.idle_o == 'h 6);

    // Inputs to DUT
    assign dut.req_i = ap_start;
    assign dut.addr_i = address_i;
    assign dut.we_i = we_i;
    assign dut.wdata_i = data_i;
    assign dut.be_i = size_i;

    // Outputs from DUT
    assign data_o = dut.rdata_o;
    assign data_valid_o = dut.valid_o;
    assign data_integrity_o = dut.rdata_intg_o;
    assign data_integrity_valid_o = dut.valid_o;
    assign error_o = dut.err_o;
    assign error_valid_o = dut.valid_o;
    assign integrity_error_o = dut.intg_err_o;
    assign integrity_error_valid_o = dut.valid_o;

    initial begin
        $display("TileLinkAdapterWrapperCall: Using real implementation");
    end
`endif
endmodule