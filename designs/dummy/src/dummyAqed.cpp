#include "aqed.h"

#include "dummyAqed.h"

namespace Aqed {
    namespace Dummy {

        void Top(Aqed::AqedInput& top_input, Aqed::AqedOutput& top_output) {
            #pragma HLS disaggregate variable=top_input
            #pragma HLS disaggregate variable=top_output
            #pragma HLS array_partition variable=top_input type=complete
            #pragma HLS array_partition variable=top_output type=complete

            static Aqed aqed;
            
            top_output = aqed(top_input);
        }
    }   
}

// Because Vivado / Vitis HLS doesn't understand namespaces...
void DummyAqedTop(Aqed::Dummy::Aqed::AqedInput& top_input, Aqed::Dummy::Aqed::AqedOutput& top_output) {
    #pragma HLS interface mode=ap_ack port=top_input
    Aqed::Dummy::Top(top_input, top_output);
}
