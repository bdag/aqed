#pragma once

#ifndef __SYNTHESIS__
#include <iostream>
#endif

#include "aqed.h"

namespace Aqed {
    namespace Dummy {
        
        struct DummyDut;
        using Aqed = Aqed<16, 16, 1, DummyDut>;

        struct DummyDut {
            static void Call(Aqed::dut_input input[Aqed::kMaxBatchSize], Aqed::dut_output output[Aqed::kMaxBatchSize]) {
                #pragma HLS inline off
                #ifndef __SYNTHESIS__
                std::cout << "Dummy DUT Call" << std::endl;
                #endif

                for (size_t i = 0; i < Aqed::kMaxBatchSize; i++) {
                    #pragma HLS unroll
                    output[i] = input[i];
                }
            }
        };

        void Top(Aqed::AqedInput& top_input, Aqed::AqedOutput& top_output);
    }
}

// Because Vivado / Vitis HLS doesn't understand namespaces...
void DummyAqedTop(Aqed::Dummy::Aqed::AqedInput& top_input, Aqed::Dummy::Aqed::AqedOutput& top_output);