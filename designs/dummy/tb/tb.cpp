#include <iostream>

#include "aqed.h"
#include "dummyAqed.h"

namespace Dut = Aqed::Dummy;
using AqedDut = Dut::Aqed;

int main() {
    using std::cout;
    using std::endl;

    AqedDut::AqedInput input;
    AqedDut::AqedOutput output;

    cout << "=======================" << endl;
    cout << "=== A-QED Testbench ===" << endl;
    cout << "=======================" << endl;
    cout << "# A-QED Parameters" << endl;
    AqedDut::PrintConstants();
    cout << endl;

    cout << "# INPUT 1" << endl;
    input.is_original     = true;
    input.original_index  = 0;
    input.is_duplicate    = false;
    input.duplicate_index = 0;
    input.dut_in[0]       = 0xBDA6;
    input.Print();
    cout << endl;
    
    cout << "# A-QED CALL 1" << endl;
    DummyAqedTop(input, output);
    cout << endl;

    cout << "# OUTPUT 1" << endl;
    output.Print();
    cout << endl;

    cout << "# INPUT 2" << endl;
    input.is_original = false;
    input.is_duplicate = false;
    input.dut_in[0] = 0x5678;
    input.Print();
    cout << endl;

    cout << "# A-QED CALL 2" << endl;
    DummyAqedTop(input, output);
    cout << endl;

    cout << "# OUTPUT 2" << endl;
    output.Print();
    cout << endl;

    cout << "# INPUT 3" << endl;    
    input.is_original = false;
    input.is_duplicate = true;
    input.dut_in[0] = 0xBDA6;
    input.Print();
    cout << endl;

    cout << "# A-QED CALL 3" << endl;
    DummyAqedTop(input, output);
    cout << endl;

    cout << "# OUTPUT 3" << endl;
    output.Print();

    return (output.is_fc) ? 0 : 1;
}