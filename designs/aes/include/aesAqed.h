#pragma once

#ifndef __SYNTHESIS__
#include <iostream>
#endif

#include "aqed.h"

#include <stdint.h>

#include <ap_int.h>

#include "buf4.cpp"

namespace Aqed {
    namespace Aes {
        
        struct AesDut;
        using Aqed = Aqed<256 + 16, 16, 2, AesDut>;

        struct AesDut {
            static void Call(Aqed::dut_input input[Aqed::kMaxBatchSize], Aqed::dut_output output[Aqed::kMaxBatchSize]) {
                #pragma HLS inline off
                #pragma HLS array_partition variable=input type=complete
                #pragma HLS array_partition variable=output type=complete
                
                #ifndef __SYNTHESIS__
                std::cout << "Aes Call" << std::endl;
                #endif
                
                uint8_t key_buf[32];
                uint8_t io_buf[4];

                // Copy key
                for (int i = 0; i < 32; i++) {
                    #pragma HLS unroll
                    key_buf[i] = input[0]((i+1)*8 - 1, i*8);
                }

                // Copy input data
                for (int i = 0; i < 2; i++) {
                    #pragma HLS unroll
                    for (int j = 0; j < 2; j++) {
                        #pragma HLS unroll
                        io_buf[i*2 + j] = input[i]((j+1)*8 - 1 + 256, j*8 + 256);
                    }
                }

                // Workload
                workload(key_buf, io_buf, 4);

                // Copy output data
                for (int i = 0; i < 2; i++) {
                    #pragma HLS unroll
                    for (int j = 0; j < 2; j++) {
                        #pragma HLS unroll
                        output[i]((j+1)*8 - 1, j*8) = io_buf[i*2 + j];
                    }
                }
            }
        };

        void Top(Aqed::AqedInput& top_input, Aqed::AqedOutput& top_output);
    }
}

// Because Vivado / Vitis HLS doesn't understand namespaces...
void AesAqedTop(Aqed::Aes::Aqed::AqedInput& top_input, Aqed::Aes::Aqed::AqedOutput& top_output);