open_project vitis -reset

set cflags      "-I../../include -Iinclude"
set csimflags   "-I../../include -Iinclude"

add_files -cflags $cflags -csimflags $csimflags src/aesAqed.cpp

open_solution default -reset
set_part artix7
set_top AesAqedTop
config_rtl -reset all -reset_async
config_interface -register_io scalar_all

#csim_design 
csynth_design
#cosim_design

exit
