#include "aqed.h"

#include "aesAqed.h"

namespace Aqed {
    namespace Aes {

        void Top(Aqed::AqedInput& top_input, Aqed::AqedOutput& top_output) {
            #pragma HLS disaggregate variable=top_input
            #pragma HLS disaggregate variable=top_output
            #pragma HLS array_partition variable=top_input type=complete
            #pragma HLS array_partition variable=top_output type=complete

            static Aqed aqed;
            
            top_output = aqed(top_input);
        }
    }   
}

// Because Vivado / Vitis HLS doesn't understand namespaces...
void AesAqedTop(Aqed::Aes::Aqed::AqedInput& top_input, Aqed::Aes::Aqed::AqedOutput& top_output) {
    #pragma HLS interface mode=ap_ack port=top_input
    Aqed::Aes::Top(top_input, top_output);
}
