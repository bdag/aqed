#pragma once

#include <cstddef>

#include <ap_int.h>

/* Simulation only */
#ifndef __SYNTHESIS__
#include <cassert>
#include <iostream>
#endif


static constexpr unsigned BitsNeededStep(unsigned n) {
    return n <= 1 ? 0 : 1 + BitsNeededStep((n + 1) >> 1);
}

static constexpr unsigned BitsNeeded(unsigned n) {
    return n <= 1 ? n : BitsNeededStep(n);
}

namespace Aqed {
    using size_t  = std::size_t;
    using ap_bool = ap_uint<1>;

    /**
     * @brief A-QED
     * TODO: Long description
     * @tparam input_size       Size of DUT input in bits
     * @tparam output_size      Size of DUT output in bits
     * @tparam max_batch_size   Maximum batch size to consider
     * @tparam DUT              Class encapsulating DUT / accelerator. Must implement static methods ap_bool DUT::InputReady(), ap_bool DUT::OutputValid(), void DUT::Call(Aqed<...>::Batch* batch)
     */
    template<size_t input_size, size_t output_size, size_t max_batch_size, typename DUT>
    struct Aqed {
        /******************
        * Class Constants *
        *******************/
        static constexpr size_t kInputSize     = input_size;
        static constexpr size_t kOutputSize    = output_size;
        static constexpr size_t kMaxBatchSize  = max_batch_size;
        static constexpr size_t kBatchSizeBits = BitsNeeded(kMaxBatchSize);

        /*******************
        * Type Definitions *
        ********************/
        using dut_input  = ap_uint<input_size>;
        using dut_output = ap_uint<output_size>;
        using batch_size = ap_uint<kBatchSizeBits>;

        /********
        * State *
        *********/
        dut_input  original_input;
        dut_output original_output;

        size_t current_batch_index   = 0;
        size_t original_batch_index  = -1;
        size_t duplicate_batch_index = -1;

        size_t output_batch_index = 0;
        
        batch_size original_index  = 0;
        batch_size duplicate_index = 0;

        ap_bool is_original_labelled  = 0;
        ap_bool is_duplicate_labelled = 0;

        ap_bool is_original_done  = 0;
        ap_bool is_duplicate_done = 0;

        ap_bool is_fc = 0;

        /**************************
        * Input-Output Interfaces *
        ***************************/
        /**
         * @brief A-QED input interface
         * TODO: Long description
         */
        struct AqedInput {
            ap_bool    is_original     = 0;
            batch_size original_index  = 0;
            ap_bool    is_duplicate    = 0;
            batch_size duplicate_index = 0;
            dut_input  dut_in[kMaxBatchSize];

            #ifndef __SYNTHESIS__
            /**
             * @brief Print input
             */
            void Print() const {
                using std::cout;
                using std::endl;

                cout << "is_original     = " << is_original << endl;
                cout << "original_index  = " << original_index << endl;
                cout << "is_duplicate    = " << is_duplicate << endl;
                cout << "duplicate_index = " << duplicate_index << endl;
                for (size_t i = 0; i < kMaxBatchSize; i++) {
                    cout << "dut_in[" << i << "] = " << std::hex << dut_in[i] << std::dec << endl;
                }
            }
            #endif

            AqedInput() = default;
        };

        /**
         * @brief A-QED output interface
         * TODO: Long description
         */
        struct AqedOutput {
            ap_bool is_original_labelled  = 0;
            ap_bool is_original_done      = 0;
            ap_bool is_duplicate_labelled = 0;
            ap_bool is_duplicate_done     = 0;
            ap_bool is_fc                 = 0;
            dut_output dut_out[kMaxBatchSize];

            #ifndef __SYNTHESIS__
            /**
             * @brief Print output
             */
            void Print() const {
                using std::cout;
                using std::endl;
                
                cout << "is_original_labelled  = " << is_original_labelled << endl;
                cout << "is_original_done      = " << is_original_done << endl;
                cout << "is_duplicate_labelled = " << is_duplicate_labelled << endl;
                cout << "is_duplicate_done     = " << is_duplicate_done << endl;
                cout << "is_fc                 = " << is_fc << endl;
                for (size_t i = 0; i < kMaxBatchSize; i++) {
                    cout << "dut_out[" << i << "] = " << std::hex << dut_out[i] << std::dec << endl;
                }
            }
            #endif

            AqedOutput() = default;
        };

        #ifndef __SYNTHESIS__
        /**
         * @brief Print A-QED class constants
         * TODO: Long description
         */
        static void PrintConstants() {
            using std::cout;
            using std::endl;

            cout << "kInputSize       = " << kInputSize << endl;
            cout << "kOutputSize      = " << kOutputSize << endl;
            cout << "kMaxBatchSize    = " << kMaxBatchSize << endl;
        }
        #endif

        /******************
        * A-QED Functions *
        *******************/
        /**
         * @brief aqed_in
         * TODO: Long description
         * @param input 
         */
        void aqed_in(AqedInput& input) {
            #pragma HLS inline off
            ap_bool do_label_original = input.is_original && (input.original_index < kMaxBatchSize) && !is_original_labelled;
            
            ap_bool temp_1 = input.is_duplicate && (input.duplicate_index < kMaxBatchSize) && !is_duplicate_labelled;
            ap_bool temp_2 = is_original_labelled && (input.dut_in[input.duplicate_index] == original_input);
            ap_bool temp_3 = do_label_original && (input.dut_in[input.original_index] == input.dut_in[input.duplicate_index]);

            ap_bool do_label_duplicate = temp_1 && (temp_2 || temp_3);

            if (do_label_original) {
                is_original_labelled = 1;
                original_input = input.dut_in[input.original_index];
                original_batch_index = current_batch_index;
                original_index = input.original_index;
            }

            if (do_label_duplicate) {
                is_duplicate_labelled = 1;
                duplicate_batch_index = current_batch_index;
                duplicate_index = input.duplicate_index;
            }

            current_batch_index++;
        }

        /**
         * @brief aqed_out
         * TODO: Long description
         * @return AqedOutput 
         */
        void aqed_out(AqedOutput& output) {
            #pragma HLS inline off
            dut_output duplicate_output;

            is_original_done = is_original_labelled && (output_batch_index >= original_batch_index);
            if (is_original_done && (output_batch_index == original_batch_index) && !is_duplicate_done) {
                original_output = output.dut_out[original_index];
            }

            if (is_original_labelled && is_duplicate_labelled && (output_batch_index == duplicate_batch_index) && !is_duplicate_done) {
                is_duplicate_done = 1;
                duplicate_output = output.dut_out[duplicate_index];
                is_fc = (original_output == duplicate_output);
            }

            if (output_batch_index > duplicate_batch_index) {
                is_duplicate_done = 1;
            }

            output_batch_index++;
        }

        /**
         * @brief A-QED call
         * TODO: Long description
         * @param input 
         * @return AqedOutput 
         */
        AqedOutput operator()(AqedInput& input) {
            static AqedOutput output;

            // Monitor input sequence
            aqed_in(input);

            // Call DUT
            DUT::Call(input.dut_in, output.dut_out);
            
            // Monitor output sequence
            aqed_out(output);
            
            output.is_original_labelled = is_original_labelled;
            output.is_original_done = is_original_done;
            output.is_duplicate_labelled = is_duplicate_labelled;
            output.is_duplicate_done = is_duplicate_done;
            output.is_fc = is_fc;

            return output;
        }
    };
}